package ro.star.app.service;

import ro.star.app.model.User;

public interface UserService {
    public User findUserByEmail(String email);
    public void saveUser(User user);
}
